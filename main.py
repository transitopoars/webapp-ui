from flask import Flask, render_template, request
import json
from flask.ext.images import Images

app = Flask(__name__)
app.secret_key = 'tprs'
images = Images(app)

@app.template_filter('get_datetime')
def get_datetime(stamp,format):
    from datetime import datetime
    date = datetime.strptime(stamp,'%Y-%m-%d %H:%M:%S')
    return date.strftime(format)

@app.route("/")
def home():
    url = request.args.get('url')

    width = request.args.get('w')
    if not width:
        width = 400

    height = request.args.get('h')
    if not height:
        height = 400

    fastconn = request.args.get('fastconn')
    if fastconn:
        if fastconn.lower() == 'false':
            fastconn = False
        else:
            fastconn = True
    else:
        fastconn = False

    stats = request.args.get('stats')
    if stats:
        if stats.lower() == 'false':
            stats = False
        else:
            stats = True
    else:
        stats = True

    if fastconn:
        quality = 85
    else:
        quality = 50

    if url:
        import requests
        r = requests.get(url)
        tweets = json.loads(r.text)
    else:
        file = open('lasttweets.json', 'r')
        tweets = json.loads(file.read())
        file.close()

    return render_template('devices.html', fastconn=fastconn, tweets=tweets, width=width, height=height, quality=quality, stats=stats, picture_w=width, icon_w=str(int(int(width)/6)))

@app.route('/direct/<path:url>')
def direct(url):
    kwargs = {}
    for key in ('width', 'height', 'mode', 'quality'):
        value = request.args.get(key) or request.args.get(key[0])
        if value is not None:
            value = int(value) if value.isdigit() else value
            kwargs[key] = value
    return redirect(images.build_url(url, **kwargs))

if __name__ == "__main__":
	app.debug = True
	app.run(host='0.0.0.0',port=8080)
